## Twitter simulator
#### Descripción:

Proyecto de aprendizaje en el cual se desarrolla un clon de Twitter con React.

## Proyecto

[Visitar proyecto](https://master.d1k2jls1dvyipj.amplifyapp.com/ "Twitter simulator")

## Imágenes del proyecto

![Imagen 1](https://i.ibb.co/XzgMpYJ/Screenshot-4.png")

## Instalación

Primero hay que clonar este repositorio. Es necesario tener instalado `node` and `npm` or `yarn` de manera global en su maquina.

Instalación:

`npm install` o `yarn`  

Ejecutar App:

`npm start` o `yarn start` 

Visitar App:

`localhost:3000`  

#### Conclusión:  

Este proyecto fue realizado en 1 día, con propósito de aprendizaje y como guía para futuros proyectos. 

Este proyecto fue realizado con `create-react-app`.